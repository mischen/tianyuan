<?php 
require_once '../common/config.inc.php';

/*
 * 1.收录用户提交的数据
 * 2.将用户提交的数据更新到数据库表
*/
//1.收录用户提交的数据
$id = isset($_GET['id'])?$_GET['id']:0;
if(!$id){
    echo "请给出要更新的新闻分类ID";
    exit;
}

$tname = isset($_POST['tname'])?$_POST['tname']:"";
if($tname==""){
    echo "新闻分类名称不能为空";
    exit;
}

//2.将用户提交的数据更新到数据库表
//1.编译SQL语句
$query = 'update ntype set tname=:tname where id=:id';
$statm = $pdo->prepare($query);

//2.绑定参数
$statm->bindParam(":tname", $tname);
$statm->bindParam(":id", $id);

//3.执行
$bool = $statm->execute();

if($bool){
    echo "修改成功";
}else{
    echo "修改失败";
}






























