<?php 
//退出
require_once '../common/config.inc.php';

session_destroy();

//退出成功跳转回登录页
header("location:login.php");