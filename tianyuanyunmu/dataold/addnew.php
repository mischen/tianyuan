<?php 
//添加新闻
/*
 * 1.接收用户提交的信息
 * 2.若管理员提交了缩略图,则需要将缩略图保存到指定的目录
 *   ,将缩略图保存的目录及文件名称做返回.
 * 3.将用户提交的信息保存到数据库
*/
//var_dump($_FILES);exit;
require_once "../common/config.inc.php";

isLogin();
    
//1.接收用户提交的信息
$title = $_POST['title'];
if($title==""){
    echo "新闻标题不能为空";
    exit;
}

$contents = $_POST['contents'];
if($contents==""){
    echo "新闻内容不能为空";
    exit;
}

//获取用户提交的新闻分类
$type = $_POST['type'];


/*
 *判断管理员有没有提交缩略图 * 
*/
$image = "";
if($_FILES["imgs"]["tmp_name"]!=""){    
    $toWidth = 155;
    $toHeight = 100;
    $image = newSize($_FILES["imgs"],$toWidth,$toHeight);
 }



//2.将用户提交的信息保存到数据库
//1.编译SQL语句
$query = 'insert news(title,contents,path,type,addtime,image)
          value
          (:title,:contents,:path,:type,:addtime,:image)';
$statm = $pdo->prepare($query);

//2.绑定参数
$statm->bindParam(":title", $title);
$statm->bindParam(":contents", $contents);
$path = "";
$statm->bindParam(":path", $path);

$statm->bindParam(":type", $type);
//当前新闻添加的时间,格式:时间戳
$time = time();
$statm->bindParam(":addtime", $time);
$statm->bindParam(":image",$image);
//执行
$bool = $statm->execute();
//var_dump($statm->errorInfo());
if($bool){
    echo "新增新闻成功";
}else{
    echo "新增新闻失败";
}











