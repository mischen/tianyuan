<?php
//从 news 表中 读取出 block 字段为 0 的新闻标题...
require_once '../common/config.inc.php';

//预处理 1.编译SQL语句
$query = 'select n.id,title,tname 
          from news as n inner join ntype as t
          on n.type = t.id
          where block=\'0\'';
$statm = $pdo->prepare($query);

//3.执行
$statm->execute();

//从结果集中获取数据
$rows = $statm->fetchAll(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>my demo</title>
<link type="text/css" rel="stylesheet" href="styles/reset.css" media="all"/>
<style>
    #wrap{
	     padding:20px;
    }
     table{
	      width:100%;
     	  border-top:1px solid #ccc;
     	  border-left:1px solid #ccc;
     }
     td,th{
	      border-right:1px solid #ccc;
     	  border-bottom:1px solid #ccc;
     	  padding:8px;
     }
</style>
</head>
<body>
 <div id="wrap"> 
  <form action="bathdelete.php" method="post" id="frm">
          <table>
               <tr>
                     <th colspan="5" class="title" style="font-size:30px">回收站列表</th>
               </tr>
               <tr>
                    <td><input type="checkbox" id="mark" /></td>
                    <td>新闻ID</td>
                    <td>新闻标题</td>
                    <td>新闻分类</td>
                    <td>操作</td>
               </tr> 
               <?php
                foreach($rows as $value){
               ?>
                <tr>
                <td><input type="checkbox" name="ids[]" value="<?php echo $value['id']; ?>"></td>
                <td><?php echo $value['id']; ?></td>
                <td><?php echo $value['title']; ?></td>
                <td><?php echo $value['tname']; ?></td>
                <td><a href="restornews.php?id=<?php echo $value['id']; ?>">恢复</a> / 
                    <a href="deletenews.php?id=<?php echo $value['id'];?>">永久删除</a></td>
                </tr>
                <?php } ?>
               <tr><td colspan="5" ><input type="button" value="批量删除" id="bu1"/>&nbsp;<input type="button" value="批量恢复" id="bu2"/></td></tr>
          </table>
</form>
 </div>
<script>
var iobj = document.getElementById("mark");
var ids = document.getElementsByName("ids[]");
iobj.onclick = function(){
	if(iobj.checked){
		for(var i=0;i<ids.length;i++){
			ids[i].checked = true;
		}	
	}else{
		for(var i=0;i<ids.length;i++){
			ids[i].checked = false;
		}		
	}
}

var fobj = document.getElementById("frm");
var bobj1 =  document.getElementById("bu1");
var bobj2 =  document.getElementById("bu2");

bobj1.onclick = function(){
	 //当bu1发生单击事件的事件处理程序
	 //改变from表单的action属性值	 
	 fobj.action = "bathdelete.php";
	 //将form表单提交.
	 fobj.submit();	 
}
bobj2.onclick = function(){
	//当bu2发生单击事件的事件处理程序
	//改变from表单的action属性值	 
	fobj.action = "bathrestore.php";
	//将form表单提交.
	fobj.submit();	 
}
</script>
</body>
</html>








