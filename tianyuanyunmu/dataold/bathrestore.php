<?php 
/*
 * 批量恢复
 * 1.获取将要进行恢复的新闻id
 * 2.在news表中,将恢复的新闻block字段改为 1.
*/
require_once '../common/config.inc.php';
//1.获取将要进行恢复的新闻id
$ids = $_POST['ids'];
//$ids 是一个索引数组 索引数组中的每一个
//元素就是要进行恢复的新闻id

//1.编译SQL语句
$query = 'update news set block=\'1\' where id=?';
$statm = $pdo->prepare($query);

$successId = "";
$failureId = "";

foreach ($ids as $id){
    //2.绑定参数
    $statm->bindParam(1, $id);
    //3.执行
    $bool = $statm->execute();
    if($bool){
        $successId.="##".$id;
    }else{
        $failureId.="##".$id;
    }
}
echo "成功恢复的新闻ID:".$successId;
echo "<br/>";
echo "恢复失败的新闻ID:".$failureId;


























