<?php 
/*
 * 1.收录用户提交的信息
 * 2.更新数据库表中对应的数据记录
*/
require_once '../common/config.inc.php';

//1.收录用户提交的信息
//获取要更新新闻的id
$id = isset($_GET['id'])?$_GET['id']:0;
if(!$id){
    //$id=0的情况
    echo "请给出要更新的新闻id";
    exit;
}

//新闻标题
$title = isset($_POST['title'])?$_POST['title']:"";
if($title==""){
    echo "新闻标题不能为空";
    exit;
}

//新闻分类
$type = isset($_POST['type'])?$_POST['type']:"";

//新闻正文(内容)
$contents = isset($_POST['contents'])?$_POST['contents']:"";
if($contents==""){
    echo "新闻内容不能为空";
    exit;
}

//2.更新数据库表中对应的数据记录
//1.编译SQL语句
$query = 'update news set 
          title=:title,
          type=:type,
          contents=:contents
          where id=:id';
$statm = $pdo->prepare($query);

//2.绑定参数
$statm->bindParam(":title", $title);
$statm->bindParam(":type", $type);
$statm->bindParam(":contents", $contents);
$statm->bindParam(":id", $id);

//3.执行
$bool = $statm->execute();

if($bool){
    echo "更新成功";
}else{
    echo "更新失败";
}























