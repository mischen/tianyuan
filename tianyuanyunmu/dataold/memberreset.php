<?php
//获取会员名称
require_once '../common/config.inc.php';

//仅管理员可见
isLogin();

$id = isset($_GET['id'])?$_GET['id']:0;
if(!$id){
    echo "请给出要重置密码的用户ID";
    exit;
}


//获取当前用户的用户名
$query = 'select username from user where id=?';
$statm = $pdo->prepare($query);

//绑定参数
$statm->bindParam(1, $id);

//执行
$statm->execute();

//获取数据
$row = $statm->fetch(PDO::FETCH_ASSOC);
//$row['username']
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>my demo</title>
<link type="text/css" rel="stylesheet" href="styles/reset.css" media="all"/>
<style>
    #wrap{
	     padding:20px;
    }
     table{
	      width:100%;
     	  border-top:1px solid #ccc;
     	  border-left:1px solid #ccc;
     }
     td,th{
	      border-right:1px solid #ccc;
     	  border-bottom:1px solid #ccc;
     	  padding:8px;
     }
</style>
</head>
<body>
 <div id="wrap">
    <form action="memberresetdo.php?id=<?php echo $id; ?>" method="post">
    	<input type="hidden" name="id" value="<?php echo $id; ?>">
          <table>
               <tr>
                     <th colspan="2" class="title" style="font-size:30px">重置密码</th>
               </tr>
               <tr>
                    <td>会员名称</td>
                    <td><?php echo $row['username']; ?></td>
               </tr>
               
                <tr>
                    <td>重置密码</td>
                    <td><input type="password" name="newpwd"></td>
               </tr>
               
                <tr>
                    <td>确认密码</td>
                    <td><input type="password" name="confirm"></td>
               </tr>
               
               <tr>
                    <th colspan="2">
                        <input type="submit" value="重置"/>
                    </th>
               </tr>
          </table>
    </form>
 </div>
</body>
</html>