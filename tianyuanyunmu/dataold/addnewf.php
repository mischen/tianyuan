<?php
//从ntype数据库表中读取出内容
require_once '../common/config.inc.php';

isLogin();

//1.编译SQL语句
$query = 'select id,tname from ntype';
$statm = $pdo->prepare($query);

//3.执行
$statm->execute();

//从结果集中获取查询的结果
$rows = $statm->fetchAll(PDO::FETCH_ASSOC);

//var_dump($rows);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>my demo</title>
<link type="text/css" rel="stylesheet" href="styles/reset.css" media="all"/>
<style>
    #wrap{
	     padding:20px;
    }
     table{
	      width:100%;
     	  border-top:1px solid #ccc;
     	  border-left:1px solid #ccc;
     }
     td,th{
	      border-right:1px solid #ccc;
     	  border-bottom:1px solid #ccc;
     	  padding:8px;
     }
</style>
</head>
<body>
 <div id="wrap">
    <form action="addnew.php" method="post" enctype="multipart/form-data">
          <table>
               <tr>
                     <th colspan="2" class="title" style="font-size:30px">新闻发布</th>
               </tr>
               <tr>
                    <td>标题</td>
                    <td><input   type="text" name="title"/></td>
               </tr>
               <tr>
                    <td>分类</td>
                    <td>
                         <select name="type">
                             <?php
                             foreach ($rows as $value){
                             ?>
                              	<option value="<?php echo $value['id']; ?>"><?php echo $value['tname']; ?></option>
                             <?php 
                             }                             
                             ?>
                         </select>
                    </td>
               </tr>
               <tr>
                    <td>缩略图</td>
                    <td>
                         <input type="file" name="imgs" />
                    </td>
               </tr>
               <tr>
                    <td>正文</td>
                    <td>
                        <textarea rows="10" cols="50" name="contents"></textarea>
                    </td>
               </tr>
               <tr>
                     <th colspan="2">
                         <input type="submit" value="发布"/>
                     </th>
               </tr>
          </table>
    </form>
 </div>
</body>
</html>