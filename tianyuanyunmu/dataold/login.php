<?php
/*
 * 1.收录用户提交的数据
 * 2.将用户提交的数据去数据库表(admin)表进行比对.
 */
require_once '../common/config.inc.php';
/*
 * $_POST 当用户没有点击提交时,是一个空数组
 * 当用户点击了提交,$_POST都不会是空数组
 */
if($_POST){
    //1.收录用户提交的验证码
    $code = $_POST['code'];
    //验证验证码不能为空
    if($code==""){
        echo "验证码不能为空";
        exit;
    }
    
    //2.将用户提交的验证码 与 session中的验证码进行比对
    if($code!=$_SESSION['vcode']){
        echo "验证码错误";
        exit;
    }
    
    //1.收录用户提交的数据
    $aname = $_POST['aname'];
    if($aname==""){
        echo "管理员账号不能为空";
        exit;
    }
    
    $password = $_POST['password'];
    if($password==""){
        echo "管理员密码不能为空";
        exit;
    }
    //2.将用户提交的数据去数据库表(admin)表进行比对.
    //1.编译SQL语句
    $query = 'select id from admin 
              where aname=:aname and password=:password';
    $statm = $pdo->prepare($query);
    
    //2.绑定参数
    $statm->bindParam(':aname', $aname);
    $password = md5($password);
    $statm->bindParam(':password', $password);
    
    //3.执行
    $bool = $statm->execute();
    
    //获取结果集的行数
    $rowCount = $statm->rowCount();
    if($rowCount==1){
        //获取用户id
        $row = $statm->fetch(PDO::FETCH_ASSOC);        
        //$row = array("id"=>1);
        //登录成功
        //将当期登录成功的用户的id和用户名保存到session中
        $_SESSION['id'] = $row['id'];
        $_SESSION['aname'] = $aname;
        //跳转到 index1.php文件
        header("location:index1.php");
    }else{
        //登录失败
        echo "登录失败";
    }
    
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>my demo</title>
<link type="text/css" rel="stylesheet" href="styles/reset.css" media="all"/>
<style>
    #wrap{
	     padding:20px;
    }
     table{
	      width:100%;
     	  border-top:1px solid #ccc;
     	  border-left:1px solid #ccc;
     }
     td,th{
	      border-right:1px solid #ccc;
     	  border-bottom:1px solid #ccc;
     	  padding:8px;
     }
</style>
</head>
<body>
 <div id="wrap">
    <form action="" method="post">
          <table>
               <tr>
                     <th colspan="2" class="title" style="font-size:30px">系统用户登录</th>
               </tr>
               <tr>
                    <td>管理员账号</td>
                    <td><input  type="text" name="aname"/></td>
               </tr>
               <tr>
                    <td>管理员密码</td>
                    <td><input  type="password" name="password"/></td>
               </tr>
               
               <tr>
                    <td>验证码</td>
                    <td><input  type="text" name="code"/>&nbsp;
                    <img src="../common/code.php" id="vcode"></td>
               </tr>
               
               <tr>
                    <th colspan="2">
                        <input type="submit" value="登录"/>
                    </th>
               </tr>
          </table>
    </form>
 </div>
<script>
//获取对象
var iobj = document.getElementById("vcode");
iobj.onclick = function(){
	iobj.src = "../common/code.php?rand="+Math.random();
}
</script> 
</body>
</html>











