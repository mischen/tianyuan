<?php
require_once '../common/config.inc.php';

isLogin();

/*
 * 1.收录用户提交的数据
 */
if($_POST){
    $password = $_POST['password'];
    if($password==""){
        echo "原密码不能为空";
        exit;
    }
    
    $newpwd= $_POST['newpwd'];
    if($newpwd==""){
        echo "新密码不能为空";
        exit;
    }
    
    $confirm= $_POST['confirm'];
    if($confirm==""){
        echo "确认密码不能为空";
        exit;
    }
    
    //2.验证
    //新密码是否与确认密码一致
    if($newpwd!=$confirm){
        echo "新密码与确认密码不一致";
        exit;
    }
    
    //验证原密码是否正确--数据库比对
    $query = 'select id from admin 
              where password=:password and aname=:aname';
    $statm = $pdo->prepare($query);
    
    //绑定参数
    $password = md5($password);
    $statm->bindParam(":password", $password);
    $statm->bindParam(":aname", $_SESSION['aname']);
    
    //执行
    $statm->execute();
    
    //获取结果集的行数
    $rowCount = $statm->rowCount();
    if($rowCount<1){
        echo "原密码错误";
        exit;
    }
    
    //重置密码
    $query = 'update admin set password=:password where id=:id';
    $statm = $pdo->prepare($query);
    
    //绑定参数
    $newpwd = md5($newpwd);
    $statm->bindParam(":password", $newpwd);
    $statm->bindParam(":id", $_SESSION['id']);
    
    //执行
    $bool = $statm->execute();
    if($bool){
        echo "重置密码成功";
        exit;
    }else{
        echo "重置密码失败";
        exit;
    }
    
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>my demo</title>
<link type="text/css" rel="stylesheet" href="styles/reset.css" media="all"/>
<style>
    #wrap{
	     padding:20px;
    }
     table{
	      width:100%;
     	  border-top:1px solid #ccc;
     	  border-left:1px solid #ccc;
     }
     td,th{
	      border-right:1px solid #ccc;
     	  border-bottom:1px solid #ccc;
     	  padding:8px;
     }
</style>
</head>
<body>
 <div id="wrap">
    <form action="" method="post">
          <table>
               <tr>
                     <th colspan="2" class="title" style="font-size:30px">重置密码</th>
               </tr>
               <tr>
                    <td>原密码</td>
                    <td><input   type="password" name="password"/></td>
               </tr>
               
               <tr>
                    <td>新密码</td>
                    <td><input   type="password" name="newpwd"/></td>
               </tr>
               
               <tr>
                    <td>确认密码</td>
                    <td><input   type="password" name="confirm"/></td>
               </tr>
               <tr>
                     <th colspan="2">
                         <input type="submit" value="重置"/>
                     </th>
               </tr>
          </table>
    </form>
 </div>
</body>
</html>


