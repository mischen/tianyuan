<?php 
//会员列表
//将user表的数据从数据库表中读取出来
require_once '../common/config.inc.php';
require_once '../common/Page.class.php';

//当前页面是会员列表页,仅管理员可见
isLogin();

//每页显示记录数
$pageSize = 5;

//获取总记录数
$query = 'select id from user';
$statm = $pdo->prepare($query);
$statm->execute();

//结果集的行数
//总记录数
$total = $statm->rowCount();

//实例化分页类
$page = new Page($pageSize,$total);

$offset = $page->offset;
$query = 'select id,username,status from user limit '.$offset.','.$pageSize;
$statm = $pdo->prepare($query);

$statm->execute();

//从结果集获取数据
$rows = $statm->fetchAll(PDO::FETCH_ASSOC);
//var_dump($rows);
foreach ($rows as $key=>$value){
    if($value['status']==1){
        $rows[$key]['status_str'] = "未锁定";
    }else{
        $rows[$key]['status_str'] = "锁定";
    }
}
//var_dump($rows);exit;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>my demo</title>
<script src="js/jquery-1.8.3.js"></script>
<link type="text/css" rel="stylesheet" href="styles/reset.css" media="all"/>
<style>
    #wrap{
	     padding:20px;
    }
     table{
	      width:100%;
     	  border-top:1px solid #ccc;
     	  border-left:1px solid #ccc;
     }
     td,th{
	      border-right:1px solid #ccc;
     	  border-bottom:1px solid #ccc;
     	  padding:8px;
     }
</style>
</head>
<body>
 <div id="wrap">  
          <table>
               <tr>
                     <th colspan="4" class="title" style="font-size:30px">会员列表</th>
               </tr>
               <tr>
                    <td>会员ID</td>
                    <td>会员名称</td> 
                    <td>状态</td>
                    <td>操作</td>                     
               </tr> 
                <?php
                foreach ($rows as $value){
                ?>        
                <tr>
                <td><?php echo $value['id']; ?></td>
                <td><?php echo $value['username']; ?></td> 
                <td><span id="status_<?php echo $value['id']; ?>" onclick="changeStatus(<?php echo $value['id']; ?>);"><?php echo $value['status_str']; ?></span></td> 
                <td><a href="memberreset.php?id=<?php echo $value['id']; ?>">重置密码</a></td>              
                </tr>
                <?php 
                    }
                ?>
                <tr>
                <td colspan="4">
                <?php $page->pages(); ?>
                </td>
                </tr>
              
         </table>
 </div>
 <script type="text/javascript">
 function changeStatus(id){
	 //获取到当前要更状态的会员id
	 //拼装状态的span的id
	 var spanId = "status_"+id;
	 //通过spanId获取对象.获取文本
	 var sObj = document.getElementById(spanId); 
	 var statusStr = sObj.innerHTML;
	 var status = 0;
	 //对于 statusStr 进行翻译
	 if(statusStr=="未锁定"){
		 status = 1;
     }
     //应用AJAX 向memberstatus.php程序抛送数据
     //引入 jQuery库
     $.ajax({
         "url":"memberstatus.php",
         "type":"post",
         "data":{"id":id,"status":status},
         "success":function(response){            
             var m = $.parseJSON(response);
             	sObj.innerHTML=m.message;
             }
         });
	 
 }
 </script>
</body>
</html>





















