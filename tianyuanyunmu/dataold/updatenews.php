<?php
//从ntype表中读取出新闻分类
require_once '../common/config.inc.php';

$id = isset($_GET['id'])?$_GET['id']:0;
if(!$id){
    //当$id=0;
    echo "请给出要更新的新闻id";
    exit;
}


//1.编译SQL语句
$query = 'select id,tname from ntype';
$statm = $pdo->prepare($query);

//3.执行
$statm->execute();

//获取查询的结果
$rows = $statm->fetchAll(PDO::FETCH_ASSOC);
/*
 * $rows = array(
 *         0=>array("id"=>1,"tname"=>"国际")
 *         1=>array("id"=>2,"tname"=>"国内") 
 *         );
 */
//从新闻表(news)读取出一条新闻
//1.编译SQL语句
//Notice: Undefined index: id in  on line 22


$query = 'select id,title,contents,type
          from news
          where id=?';
$statm = $pdo->prepare($query);

//2.绑定参数
$statm->bindParam(1, $id);

//3.执行
$statm->execute();

//从结果集中获取数据
$row = $statm->fetch(PDO::FETCH_ASSOC);
//var_dump($row);





?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>my demo</title>
<link type="text/css" rel="stylesheet" href="styles/reset.css" media="all"/>
<style>
    #wrap{
	     padding:20px;
    }
     table{
	      width:100%;
     	  border-top:1px solid #ccc;
     	  border-left:1px solid #ccc;
     }
     td,th{
	      border-right:1px solid #ccc;
     	  border-bottom:1px solid #ccc;
     	  padding:8px;
     }
</style>
</head>
<body>
 <div id="wrap">
    <form action="updatenewsdo.php?id=<?php echo $id; ?>" method="post">
          <table>
               <tr>
                     <th colspan="2" class="title" style="font-size:30px">新闻更新</th>
               </tr>
               <tr>
                    <td>标题</td>
                    <td><input   type="text" name="title" value="<?php echo $row['title'];?>"/></td>
               </tr>
               <tr>
                    <td>分类</td>
                    <td>
                         <select name="type">
                               <?php foreach ($rows as $value){  ?>
                              	<option value="<?php echo $value['id'];?>" 
                              	<?php if($row['type']==$value['id']){ ?>
                              	selected="selected" 
                              	<?php } ?>>
                              	<?php echo $value['tname'];?></option>
                               <?php } ?>
                         </select>
                    </td>
               </tr>
               <tr>
                    <td>正文</td>
                    <td>
                        <textarea rows="10" cols="50" name="contents"><?php echo $row['contents'];?></textarea>
                    </td>
               </tr>
               <tr>
                     <th colspan="2">
                         <input type="submit" value="更新"/>
                     </th>
               </tr>
          </table>
    </form>
 </div>
</body>
</html>




