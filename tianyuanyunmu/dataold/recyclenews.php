<?php 
//将指定id的新闻设置为回收状态
require_once '../common/config.inc.php';

//指定要回收的新闻id
$id = isset($_GET['id'])?$_GET['id']:0;
if(!$id){
    echo "请给出要放入回收站的新闻id";
    exit;
}

//1.编译SQL语句
$query = 'update news set block=\'0\' where id=?';
$statm = $pdo->prepare($query);

//2.绑定参数
$statm->bindParam(1, $id);

//3.执行
$bool = $statm->execute();

if($bool){
    echo "放入回收站成功";
}else{
    echo "放入回收站失败";
}













