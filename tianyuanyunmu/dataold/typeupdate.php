<?php
//将要修改的新闻分类id对应的新闻分类名称从数据库读取出来
require_once '../common/config.inc.php';

//获取新闻分类id
$id = isset($_GET['id'])?$_GET['id']:0;
if(!$id){
    echo '请给出要修改的新闻分类ID';
    exit;
}

//从数据库表中将对应新闻分类id的分类名称读取出来
//1.编译SQL语句
$query = 'select tname from ntype where id=?';
$statm = $pdo->prepare($query);

//2.绑定参数
$statm->bindParam(1, $id);

//3.执行
$statm->execute();

//从结果集中获取数据
$row = $statm->fetch(PDO::FETCH_ASSOC);

//var_dump($row);



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>my demo</title>
<link type="text/css" rel="stylesheet" href="styles/reset.css" media="all"/>
<style>
    #wrap{
	     padding:20px;
    }
     table{
	      width:100%;
     	  border-top:1px solid #ccc;
     	  border-left:1px solid #ccc;
     }
     td,th{
	      border-right:1px solid #ccc;
     	  border-bottom:1px solid #ccc;
     	  padding:8px;
     }
</style>
</head>
<body>
 <div id="wrap">
    <form action="typeupdatedo.php?id=<?php echo $id; ?>" method="post">
          <table>
               <tr>
                     <th colspan="2" class="title" style="font-size:30px">修改新闻分类</th>
               </tr>
               <tr>
                    <td>新闻分类</td>
                    <td><input   type="text" name="tname" value="<?php echo $row['tname']; ?>"/></td>
               </tr>
               
               <tr>
                     <th colspan="2">
                         <input type="submit" value="修改"/>
                     </th>
               </tr>
          </table>
    </form>
 </div>
</body>
</html>