<?php
//将admin数据库表中的数据读取出来
require_once '../common/config.inc.php';

//预处理 从admin表中读取出数据
//1. 编译SQL语句
$query = 'select id,aname from admin';
$statm = $pdo->prepare($query);

//3.执行
$statm->execute();

//从结果集中获取数据
$rows = $statm->fetchAll(PDO::FETCH_ASSOC);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>my demo</title>
<link type="text/css" rel="stylesheet" href="styles/reset.css" media="all"/>
<style>
    #wrap{
	     padding:20px;
    }
     table{
	      width:100%;
     	  border-top:1px solid #ccc;
     	  border-left:1px solid #ccc;
     }
     td,th{
	      border-right:1px solid #ccc;
     	  border-bottom:1px solid #ccc;
     	  padding:8px;
     }
</style>
</head>
<body>
 <div id="wrap">  
          <table>
               <tr>
                     <th colspan="3" class="title" style="font-size:30px">系统用户列表</th>
               </tr>
               <tr>
                    <td>管理员ID</td>
                    <td>管理员</td> 
                    <td>操作</td>                     
               </tr> 
               <?php foreach($rows as $value){ ?>            
                <tr>
                <td><?php echo $value['id']; ?></td>
                <td><?php echo $value['aname']; ?></td> 
                <td><a href="admindelete.php?id=<?php echo $value['id']; ?>">删除</a></td>              
                </tr>
               <?php } ?> 
         </table>
 </div>
</body>
</html>








