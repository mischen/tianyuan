<?php 
//将已经处在回收状态下的新闻 删除掉
require_once '../common/config.inc.php';

//1.编译SQL语句
$query = 'delete from news where id=?';
$statm = $pdo->prepare($query);

$id = isset($_GET['id'])?$_GET['id']:0;
if(!$id){
    //当id为0时.
    echo "请给出要永久删除的新闻ID";
    exit;
}

//2.绑定参数
$statm->bindParam(1, $id);

//3.执行
$bool = $statm->execute();

if($bool){
    echo "删除新闻成功";
}else{
    echo "删除新闻失败";
}















