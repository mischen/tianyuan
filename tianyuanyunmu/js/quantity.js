/**
 * Created by fe on 2017/1/12.
 */
(function () {
    var CalQaty = function ($el) {
        this.$el = $el;
        this.calBtn = this.$el.find('.co-btn');
        this.numW = this.$el.find('#co-num');
        this.minBtn = this.$el.find('.co-mine');
        this.calBtn.on('click',{numW: this.numW,minBtn :this.minBtn},this.cal);
        this.numW.on('keyup',{numW: this.numW},this.reTest);
    };

    CalQaty.prototype.cal = function (e) {
        var $this = $(this);
        var $numW = e.data.numW;
        var $minBtn = e.data.minBtn;
        var num = $numW.val();
        if($this.hasClass('co-add')){
            $numW.val(++num);
            $minBtn.removeClass('forbid');
        }else if(!$this.hasClass('forbid')){
            $numW.val(--num);
            if(parseInt($numW.val())===1){
                $minBtn.addClass('forbid');
            }
        }
    };

    CalQaty.prototype.reTest = function (e) {
        var $numW = e.data.numW;
        if(!/^[1-9]\d*$/.test($numW.val())){
            $numW.val(1)
        }
    };

    new CalQaty($('.ado-counter'));
})();