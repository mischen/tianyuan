


/**
 * 保存登录信息        
 */
function saveUserInfo(user_date){
    //将数组转换为Json字符串保存在cookie中,过期时间为1天
    $.cookie("USER_INFO",$.toJSON(user_date),{ expires: 1,path:"/"}); 
    console.log($.toJSON(user_date));
}
/**
 * 获取登录用户信息
 */
function getUserInfo(){
	var user_info = $.parseJSON($.cookie("USER_INFO"));
    //从cookie中还原数组
    if(user_info==null || user_info == ""){
    	myAlert("获取用户信息失败，请稍后再试");
    	return null;
    }else{
    	return user_info;
    }
    
}