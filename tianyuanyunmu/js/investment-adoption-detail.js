$(function(){
	var per_price = 0;
	var pig_num = 1;
	var all = 1;
	var id = GetQueryString("id");
	var p_price = 0;
	$.ajax({
		type : "POST",
		url : "/projectForWebAction!showInvestingInfo.action",
		dataType : "json",
		data : {
			id:id
		},
		success : function(data) {
			console.log(data);
			$("#project_name").html(data.project_name);
			$("#project_slogan").html(data.project_slogan);
			$("#feed_days").html(data.feed_days + "天");
			$("#intended_weight").html(data.intended_weight);
			$("#exit_mode").html(data.exit_mode);
			$("#expected_profit").html(data.expected_profit);
			$("#project_price").html(data.project_price + "元/头");
			var befor=GetQueryString("befor");
			if(parseInt(befor)==1){
				$("#stock_count").html("0头");
			}else{
			$("#stock_count").html(data.stock_count  + "头");}
			$("#main_img").attr("src","/"+data.project_main_image_url);
			$("#pay_fee").html(data.project_price + "元");
			$("#pig_type").val(data.pig_type);
			$("#order_type").val("1");
			$("#co-num").val("1");
			per_price = data.project_price;
			pig_num = data.stock_count ;
			all = data.stock_count;
			p_price = data.project_price;
			if(pig_num <= 0){
				$('#co-add').attr('disabled',"true");
			}
			$('#co-mine').attr('disabled',"true");
		},
		error : function(data) {
			
		}
	});
	
	$('#co-add').bind('click', function() {
		var num = $('#co-num').val();
		console.log(pig_num);
		if(pig_num > 0){
			$("#pay_fee").html(per_price * num + "元");
			pig_num = pig_num - 1;
			if(pig_num == 0){
				$('#co-add').attr('disabled',"true");
			}
			$('#co-mine').removeAttr("disabled");
			$("#stock_count").html(pig_num  + "头");
		}else{
			$('#co-add').attr('disabled',"true");
		}
		
	});
	
	
	
	$('#co-num').change(function(){
		var num = $(this).val();
		if(num > all){
			myAlert("不能大于库存！");
		}else if(num == all){
			pig_num = 0;
			$("#pay_fee").html(per_price * num + "元");
			$("#stock_count").html(pig_num  + "头");
			$('#co-add').attr('disabled',"true");
		}else if(num == 1){
			pig_num = all - num;
			$("#pay_fee").html(per_price * num + "元");
			$("#stock_count").html(pig_num  + "头");
			$('#co-mine').attr('disabled',"true");
		}else{
			pig_num = all - num;
			$("#pay_fee").html(per_price * num + "元");
			$("#stock_count").html(pig_num  + "头");
			$('#co-add').removeAttr("disabled");
			$('#co-mine').removeAttr("disabled");
		}
		
	});
	$('#co-mine').bind('click', function() {
		var num = $('#co-num').val();
		if(num >= 1){
			pig_num = pig_num  + 1;
			if(num == 1){
				$('#co-mine').attr('disabled',"true");
			}
			$('#co-add').removeAttr("disabled");
			$("#pay_fee").html(per_price * num + "元");
			$("#stock_count").html(pig_num  + "头");
		}else{
			$('#co-mine').attr('disabled',"true");
		}
	});
	
	
	$('#co-add').blur(function(){
		$.ajax({
			type : "POST",
			url : "/memberCouponForWebAction!queryMemberCouponList.action",
			dataType : "json",
			data : {
				memberId : memberId,
				currentPage : 1,
				pageSize : 100,
				useType : "noUse",
				pigType : $("#pig_type").val(),
				orderType : $("#order_type").val(),
				conditionPrice : p_price * $('#co-num').val()
			},
			success : function(data) {
				console.log(data);
				if(data.status == "true"){
					var str = "";
					var list = data.list;
					if(list.length > 0){
						for(var n = 0; n < list.length; n ++){
							str += "<option value='" + list[n].id + "'>" + list[n].membercoupon_text +"_"+ list[n].membercoupon_title + "</option>"
						}
						$('#coupon').html(str);
					}
				}
			},
			error : function(data) {
				
			}
		})
	});

	$('#co-num').blur(function(){
		$.ajax({
			type : "POST",
			url : "/memberCouponForWebAction!queryMemberCouponList.action",
			dataType : "json",
			data : {
				memberId : memberId,
				currentPage : 1,
				pageSize : 100,
				useType : "noUse",
				pigType : $("#pig_type").val(),
				orderType : $("#order_type").val(),
				conditionPrice : p_price * $('#co-num').val()
			},
			success : function(data) {
				console.log(data);
				if(data.status == "true"){
					var str = "";
					var list = data.list;
					if(list.length > 0){
						for(var n = 0; n < list.length; n ++){
							str += "<option value='" + list[n].id + "'>" + list[n].membercoupon_text +"_"+ list[n].membercoupon_title + "</option>"
						}
						$('#coupon').html(str);
					}
				}
			},
			error : function(data) {
				
			}
		})
		console.log(user_info_head.id);
	});
	
	$('#co-mine').blur(function(){
		$.ajax({
			type : "POST",
			url : "/memberCouponForWebAction!queryMemberCouponList.action",
			dataType : "json",
			data : {
				memberId : memberId,
				currentPage : 1,
				pageSize : 100,
				useType : "noUse",
				pigType : $("#pig_type").val(),
				orderType : $("#order_type").val(),
				conditionPrice : p_price * $('#co-num').val()
			},
			success : function(data) {
				console.log(data);
				if(data.status == "true"){
					var str = "";
					var list = data.list;
					if(list.length > 0){
						for(var n = 0; n < list.length; n ++){
							str += "<option value='" + list[n].id + "'>" + list[n].membercoupon_text +"_"+ list[n].membercoupon_title + "</option>"
						}
						$('#coupon').html(str);
					}
				}
			},
			error : function(data) {
				
			}
		});
		console.log(user_info_head.id);
	});
	
	$("#sub").click(function(){
		 var user_info_head = $.parseJSON($.cookie("USER_INFO"));
		console.log($.cookie("USER_INFO"));
		if(user_info_head == "" || user_info_head == null){
			 location.href="/web/site/login.html";	
		}else{
			var befor=GetQueryString("befor");
			
			if(pig_num<=0||parseInt(befor)==1){
			myAlert("商品已售罄，请选择其它商品");	
			}else{
			window.location.href = "/web/site/pay_order.html?id="+id+"&num=1";
		}}
		/*console.log($('#coupon').val());
		var fee = $('#pay_fee').html(); 
		fee = fee.substring(0,fee.length-1);
		console.log(fee);
		$.ajax({
			type : "POST",
			url : "/orderForPhoneAction!saveOrderZCInfo.action",
			dataType : "json",
			data : {
				memberId : $.parseJSON($.cookie("USER_INFO")).id,
				productquantity : $('#co-num').val(),
				productId : id,
				paymentFee : fee,
				exitMode : 1,
				paymentMode : 2,
				couponId : $('#coupon').val()
			},
			success : function(data) {
				console.log(data);
				if(data.success == "true"){
					window.location.href = "/web/site/pay-chongzhi.html";
				}else{
					alert(data.msg);
				}
				
			},
			error : function(data) {
				alert("网络繁忙,请稍后再试...");
			}
		});*/
	});
})
	//获取参数
	function GetQueryString(name){
	     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)","i");
	     var r = window.location.search.substr(1).match(reg);
	     if(r!=null){
	    	 return  unescape(r[2]); 
	     }
	     return null;
	}