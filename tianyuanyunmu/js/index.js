/**
 * 网站首页加载数据
 * wmm
 */

$(function(){
	$.ajax({
		type : "POST",
		url : "/projectForPhoneAction!showInvestingProjectList.action",
		dataType : "json",
		data : {
		
		},
		success : function(data) {
			var list = data.nowList;
			var tiyanList=data.tiyanList;
			if(tiyanList.length > 0){     //  拼接认养订单页面
				
				for(var i=0;i<tiyanList.length;i++){
					var str = "";
					var num=(tiyanList[i].sale_count/tiyanList[i].delivery_count)*100 + "";
					
					 var nums = Math.round(num*100)/100;
					
						str +='<div class="index-content-right-sponsor overflow-hidden mt10"><div class="index-content-right-sponsor-left fl">'+
						'<div class="index-content-right-sponsor-detail overflow-hidden mb20"><h1 class="fl">'+tiyanList[i].project_name+'<small class="mt20">'+tiyanList[i].project_slogan+'</small></h1>'+
						'<p class="fl index-content-right-sponsor-detail-icon ml100"><img src="/web/images/sponsor-detail-icon1.png"><span class="green-color">四大保障 安全可靠</span></p></div>'+
						'<div class="index-content-right-sponsor-data mb30"><ul class="overflow-hidden">'+
						'<li><p class="green-color">'+tiyanList[i].expected_profit+'%</p><span>预期年化收益率</span></li>'+
						'<li><p class="green-color">'+tiyanList[i].feed_days+'</p><span>养殖时间</span></li>'+
						'<li><p class="green-color">'+tiyanList[i].expected_selling_price+'元</p><span>认养价格</span></li>'+
						'<li class="noborder"><p class="green-color">'+(tiyanList[i].delivery_count-tiyanList[i].sale_count)+'头</p><span>剩余可认养</span></li></ul></div></div>'+
						'<div class="index-content-right-sponsor-right mt30 fr text-center mr100"><span style="color: #a4a4a4;margin-bottom: 5px;display: block" class="">认养 '+nums+'%</span>'+
						'<div class="progress index-content-right-sponsor-right-progress"><div class="progress-bar green-bg-color green-bg-color" id="jindutiao'+i+'" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: '+(tiyanList[i].sale_count/tiyanList[i].delivery_count)*100+'%;"></div></div>'+
						'<a target="_blank"  class="index-content-right-sponsor-right-action green-bg-color" id="jindu'+i+'" href="/web/site/investment-adoption-detail.html?id='+tiyanList[i].id+'">立即购买</a></div></div>';
						$(".index-content-right.fr").append(str);
						
						if((tiyanList[i].delivery_count-tiyanList[i].sale_count)>0){
						/*	$("#jindu"+i).attr('href',"/web/site/investment-adoption-detail.html?id="+tiyanList[i].id); */
							}else{
								$("#jindu"+i).text("已售罄");
								$("#jindu"+i).css("background","#999999");
								
								$("#jindutiao"+i).css("background","#999999");
								}
					}
			}
			if(list.length > 0){     //  拼接认养订单页面
				/*$(".index-content-right.fr").html("");*/
				for(var i=0;i<list.length;i++){
					var str = "";
					var num=(list[i].sale_count/list[i].delivery_count)*100 + "";
					
					 var nums = Math.round(num*100)/100;
					
						str +='<div class="index-content-right-sponsor overflow-hidden mt10"><div class="index-content-right-sponsor-left fl">'+
						'<div class="index-content-right-sponsor-detail overflow-hidden mb20"><h1 class="fl">'+list[i].project_name+'<small class="mt20">'+list[i].project_slogan+'</small></h1>'+
						'<p class="fl index-content-right-sponsor-detail-icon ml100"><img src="/web/images/sponsor-detail-icon1.png"><span class="green-color">四大保障 安全可靠</span></p></div>'+
						'<div class="index-content-right-sponsor-data mb30"><ul class="overflow-hidden">'+
						'<li><p class="green-color">'+list[i].expected_profit+'%</p><span>预期年化收益率</span></li>'+
						'<li><p class="green-color">'+list[i].feed_days+'</p><span>养殖时间</span></li>'+
						'<li><p class="green-color">'+list[i].expected_selling_price+'元</p><span>认养价格</span></li>'+
						'<li class="noborder"><p class="green-color">'+(list[i].delivery_count-list[i].sale_count)+'头</p><span>剩余可认养</span></li></ul></div></div>'+
						'<div class="index-content-right-sponsor-right mt30 fr text-center mr100"><span style="color: #a4a4a4;margin-bottom: 5px;display: block" class="">认养 '+nums+'%</span>'+
						'<div class="progress index-content-right-sponsor-right-progress"><div class="progress-bar green-bg-color green-bg-color" id="jindutiaos'+i+'" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: '+(list[i].sale_count/list[i].delivery_count)*100+'%;"></div></div>'+
						'<a target="_blank"  class="index-content-right-sponsor-right-action green-bg-color" id="jindus'+i+'" href="/web/site/investment-adoption-detail.html?id='+list[i].id+'">立即购买</a></div></div>';
						$(".index-content-right.fr").append(str);
						
						if((list[i].delivery_count-list[i].sale_count)>0){
							/*$("#jindus"+i).attr('href',"/web/site/investment-adoption-detail.html?id="+list[i].id); */
							}else{
								
								$("#jindus"+i).text("已售罄");
								$("#jindus"+i).css("background","#999999");
								
								$("#jindutiaos"+i).css("background","#999999");
								}
					}}
		},
		error : function(data) {
			
		}
	});
	$(function(){
		if($("#banner_head_url").src==null){
			$("#banner_head_url").attr("src","/web/images/member/morentouxiang.png");
		}
	})

})