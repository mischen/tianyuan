(function(D) {
    D.fn.jCarouselLite = function(E) {
        E = D.extend({
            btnPrev: null,
            btnNext: null,
            btnGo: null,
            mouseWheel: false,
            auto: null,
            speed: 200,
            easing: null,
            vertical: false,
            circular: true,
            visible: 3,
            start: 0,
            scroll: 1,
            beforeStart: null,
            afterEnd: null
        }, E || {});
        return this.each(function() {
            var Y = false,
                W = E.vertical ? "top" : "left",
                T = E.vertical ? "height" : "width";
            var I = D(this),
                R = D("ul", I),
                H = D("li", R),
                Q = H.size(),
                U = E.visible;
            var O = E.start;
            var F = I.closest(".wp-effectimage_content");
            var G = F.width() - D("a.prev", F).width() - D("a.next", F).width() - 20;
            var S = C(H);
            U = Math.min(Q, Math.floor(G / S));
            if (E.circular) {
                R.prepend(H.slice(Q - U - 1 + 1).clone()).append(H.slice(0, U).clone());
                E.start += U
            }
            var J = D("li", R),
                X = J.size(),
                K = E.start;
            I.css("visibility", "visible");
            J.css({
                overflow: "visible",
                "float": E.vertical ? "none" : "left"
            });
            R.css({
                margin: "0",
                padding: "0",
                position: "relative",
                "list-style-type": "none",
                "z-index": "1"
            });
            I.css({
                overflow: "hidden",
                position: "relative",
                "z-index": "2",
                left: "0px"
            });
            var L = E.vertical ? A(J) : C(J);
            var N = L * X;
            var P = L * U;
            J.css({
                width: J.width(),
                height: J.height()
            });
            R.css(T, N + "px").css(W, -(K * L));
            I.css(T, P + "px");
            I.bind("mod_resize", function(Z) {
                var f = Z.data;
                var a = I.closest(".wp-effectimage_content");
                var b = a.width() - D("a.prev", a).width() - D("a.next", a).width() - 20;
                var c = Math.min(Q, Math.floor(b / L));
                if (c == U) {
                    return
                }
                E.start = O;
                U = c;
                var d = H.clone();
                R.empty().append(d);
                H = D("li", R);
                if (E.circular) {
                    R.prepend(H.slice(Q - U - 1 + 1).clone()).append(H.slice(0, U).clone());
                    E.start += U
                }
                P = L * U;
                K = E.start;
                J = D("li", R), X = J.size();
                N = L * X;
                J.css({
                    width: J.width(),
                    height: J.height()
                });
                R.css(T, N + "px").css(W, -(K * L));
                I.css(T, P + "px")
            });
            if (S - J.width() > 20) {
                I.trigger("mod_resize")
            }
            if (E.btnPrev) {
                D(E.btnPrev).click(function() {
                    return M(K - E.scroll)
                })
            }
            if (E.btnNext) {
                D(E.btnNext).click(function() {
                    return M(K + E.scroll)
                })
            }
            if (E.btnGo) {
                D.each(E.btnGo, function(Z, a) {
                    D(a).click(function() {
                        return M(E.circular ? E.visible + Z : Z)
                    })
                })
            }
            if (E.mouseWheel && I.mousewheel) {
                I.mousewheel(function(a, Z) {
                    return Z > 0 ? M(K - E.scroll) : M(K + E.scroll)
                })
            }
            if (E.auto) {
                setInterval(function() {
                    M(K + E.scroll)
                }, E.auto + E.speed)
            }

            function V() {
                return J.slice(K).slice(0, U)
            }

            function M(Z) {
                if (!Y) {
                    if (E.beforeStart) {
                        E.beforeStart.call(this, V())
                    }
                    if (E.circular) {
                        if (Z <= E.start - U - 1) {
                            R.css(W, -((X - (U * 2)) * L) + "px");
                            K = Z == E.start - U - 1 ? X - (U * 2) - 1 : X - (U * 2) - E.scroll
                        } else {
                            if (Z >= X - U + 1) {
                                R.css(W, -((U) * L) + "px");
                                K = Z == X - U + 1 ? U + 1 : U + E.scroll
                            } else {
                                K = Z
                            }
                        }
                    } else {
                        if (Z < 0 || Z > X - U) {
                            return
                        } else {
                            K = Z
                        }
                    }
                    Y = true;
                    R.animate(W == "left" ? {
                        left: -(K * L)
                    } : {
                        top: -(K * L)
                    }, E.speed, E.easing, function() {
                        if (E.afterEnd) {
                            E.afterEnd.call(this, V())
                        }
                        Y = false
                    });
                    if (!E.circular) {
                        D(E.btnPrev + "," + E.btnNext).removeClass("disabled");
                        D((K - E.scroll < 0 && E.btnPrev) || (K + E.scroll > X - U && E.btnNext) || []).addClass("disabled")
                    }
                }
                return false
            }
        })
    };

    function B(E, F) {
        return parseInt(D.css(E[0], F)) || 0
    }

    function C(E) {
        return E[0].offsetWidth + B(E, "marginLeft") + B(E, "marginRight")
    }

    function A(E) {
        return E[0].offsetHeight + B(E, "marginTop") + B(E, "marginBottom")
    }
})(jQuery);