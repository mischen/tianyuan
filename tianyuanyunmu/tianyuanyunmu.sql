设置中文模式
set names gbk;
//创建天缘云牧库
CREATE DATABASE tianyuanyunmu CHARSET=UTF8;
use tianyuanyunmu;
//用户表
//注册功能 login.php
用户ID主键
用户手机号
用户密码
注册时间
CREATE TABLE tyym_user(
 uid   INT PRIMARY KEY AUTO_INCREMENT,
 uphone VARCHAR(20),
 upwd  VARCHAR(32),
 register_time DATETIME
);
INSERT INTO tyym_user VALUES
(null,'13026612222','123456',now());
INSERT INTO tyym_user VALUES
(null,'13026613333','123456',now());
INSERT INTO tyym_user VALUES
(null,'13026615555','123456',now());
INSERT INTO tyym_user VALUES
(null,'13026616666','123456',now());
INSERT INTO tyym_user VALUES
(null,'13026616667','123456',now());
//实名认证表
//实名认证功能 添加 查询 修改 add_certification.php //certification.php re_certification.php
实名认证主键
用户ID 关联用户表
实名人名字
用户身份证号码
银行卡卡号
银行卡类别
认证时间
CREATE TABLE user_certification(
 fid   INT PRIMARY KEY AUTO_INCREMENT,
 uid INT,
uname VARCHAR(100),
u_card INT,
bank_card INT,
#1工商银行 2 建设银行 3 农业银行 4 中国银行 5 交通银行 6 招商银行 7 邮政储蓄银行 8 兴业银行 9 中信银行 10 民生银行 11光大银行 12 华夏银行 13 平安银行
14 广发银行 15 农业银行 16 招商银行 17 交通银行
bank_class tinyint(1),
certification_time DATETIME
);
INSERT INTO user_certification VALUES
(null,1,'小马',2324645412313212,21231231231231,0,now());
INSERT INTO user_certification VALUES
(null,2,'小刘',2324645412313212,21231231231231,1,now());
INSERT INTO user_certification VALUES
(null,3,'小黑',2324645412313212,21231231231231,3,now());
INSERT INTO user_certification VALUES
(null,1,'小马',2324645412313212,21231231231231,2,now());

//用户详情表
//增 查
用户详情ID主键
用户ID 关联用户表
会员等级
//增  删 查 改
会员分类
//增  删 查 改
用户地址
//增  删 查 改
用户微信号
//增  删 查 改
用户邮箱
//增  删 查 改
//用户头像

CREATE TABLE user_details(
 did   INT PRIMARY KEY AUTO_INCREMENT,
 uid INT,
garde INT,
#1 普通用户 2 VIP客户 3 大客户
menber_class tinyint(1) DEFAULT 0,
u_address VARCHAR(1000),
u_weicha VARCHAR(100),
u_mail VARCHAR(100)
);
INSERT INTO user_details VALUES(null,1,1,2,'江西','一直下','das@163.com');
INSERT INTO user_details VALUES(null,2,1,2,'浙江','一只小鸟','das@163.com');
INSERT INTO user_details VALUES(null,3,1,2,'湖南','一只鸭','das@163.com');
//用户提现表
提现主键
用户ID
提现金额
提现状态
提现时间
CREATE TABLE user_cash(
 hid   INT PRIMARY KEY AUTO_INCREMENT,
 uid INT,
 cash_price  DOUBLE(30,2),
 #1 正在提现,2 已成功 , 3已取消
 cash_state tinyint (1),
cash_time DATETIME
);
INSERT INTO user_cash VALUES(null,1,2000.00,1,now());
INSERT INTO user_cash VALUES(null,2,2000.00,2,now());
INSERT INTO user_cash VALUES(null,3,2000.00,1,now());
INSERT INTO user_cash VALUES(null,1,2000.00,3,now());
INSERT INTO user_cash VALUES(null,1,2050.00,1,now());
//用户钱包
用户钱包主键
用户ID
用户实际金额
CREATE TABLE user_purse(
 eid   INT PRIMARY KEY AUTO_INCREMENT,
 uid INT,
actual_amount DOUBLE(30,2)
);
INSERT INTO user_purse VALUES(null,1,20000.00);
INSERT INTO user_purse VALUES(null,1,20000000.12);
//用户充值表
//增 查
充值ID主键
用户ID 关联用户表
充值状态
充值金额
充值时间

CREATE TABLE user_pay(
 pid   INT PRIMARY KEY AUTO_INCREMENT,
 uid INT,
#0 已成功 ,1 未成功 , 2已取消
pay_state tinyint (1),
p_price DOUBLE(30,2),
p_time DATETIME
);
INSERT INTO user_pay VALUES(null,1,1,20000.00,now());
INSERT INTO user_pay VALUES(null,1,1,20000000.12,now());
INSERT INTO user_pay VALUES(null,2,1,2000.00,now());
//用户购物车表
//增删改查
主键
用户ID 关联用户表
产品ID 关联产品表
数量
交易状态
购买时间
CREATE TABLE user_cart(
 cid INT PRIMARY KEY AUTO_INCREMENT,
 uid INT,
 pid INT,
 count INT,
#1 已成功 ,2 待付款 , 3已取消
c_state tinyint(1),
c_time DATETIME
);
INSERT INTO user_cart VALUES(null,1,1,1,1,now());
INSERT INTO user_cart VALUES(null,1,2,2,1,now());
INSERT INTO user_cart VALUES(null,1,1,1,2,now());
INSERT INTO user_cart VALUES(null,1,2,2,1,now());
INSERT INTO user_cart VALUES(null,1,1,1,1,now());
INSERT INTO user_cart VALUES(null,1,2,2,3,now());
INSERT INTO user_cart VALUES(null,1,1,1,1,now());
INSERT INTO user_cart VALUES(null,1,2,2,2,now());

//产品表
//增（后台） 删（后台） 改（后台） 查（前台）
产品ID主键
产品名称
产品价格
产品小图片
产品年化率
产品数量
CREATE TABLE  tyym_product(
 pid   INT PRIMARY KEY AUTO_INCREMENT,
 pname VARCHAR(100),
 price DOUBLE(10,2),
 s_picture VARCHAR(100),
 p_earnings VARCHAR(100),
p_total   INT
);
INSERT INTO  tyym_product VALUES
(null,'天缘生态猪1号',2000.00,'1.jpg','10.22',100),
(null,'天缘生态猪3号',3000.00,'2.jpg','10-22',200),
(null,'天缘生态猪4号',4000.00,'3.jpg','10.22-11.22',300),
(null,'天缘生态猪5号',5000.00,'4.jpg','10.22-15',400),
(null,'天缘生态猪6号',6000.00,'5.jpg','10.22-16',500);
//产品详情表
//增（后台） 删（后台） 改（后台） 查（前台）
主键
产品ID关联产品表
产品周期
产品类别
产品大图片
CREATE TABLE  p_imagetext(
 tid   INT PRIMARY KEY AUTO_INCREMENT,
 pid   INT,
p_cycle INT,
product_class VARCHAR(100),
 L_picture VARCHAR(100)
);
INSERT INTO  p_imagetext VALUES
(null,1,120,'禽类副产品','10.jpg'),
(null,1,60,'猪肉类','11.jpg'),
(null,1,110,'猪','12.jpg');
//认养表
认养主键
用户ID
产品ID
认养时间
CREATE TABLE  user_adopt(
 aid INT PRIMARY KEY AUTO_INCREMENT,
 uid INT,
 pid INT,
adopt_time DATETIME
);
INSERT INTO user_adopt VALUES(null,1,1,now());
INSERT INTO user_adopt VALUES(null,1,2,now());
INSERT INTO user_adopt VALUES(null,1,3,now());
INSERT INTO user_adopt VALUES(null,1,4,now());
INSERT INTO user_adopt VALUES(null,1,4,now());
INSERT INTO user_adopt VALUES(null,1,3,now());
INSERT INTO user_adopt VALUES(null,1,1,now());
INSERT INTO user_adopt VALUES(null,1,2,now());
//产品众筹表
//增（后台） 删（后台） 改（后台） 查（前台）
众筹表主键
众筹价格
用户ID 关联用户
产品ID关联产品表
众筹时间
众筹所需人数
已经加入众筹的人数
CREATE TABLE  p_crowdfunding(
 cid   INT PRIMARY KEY AUTO_INCREMENT,
crow_price DOUBLE(10,2),
uid   INT,
 pid   INT,
need  INT,
jion  INT,
j_time DATETIME
);
INSERT INTO  p_crowdfunding VALUES
(null,100,1,2,100,70,now()),
(null,50,2,2,100,100,now()),
(null,100,1,2,100,70,now()),
(null,50,2,2,100,100,now()),
(null,100,1,2,100,70,now()),
(null,50,2,2,100,100,now()),
(null,110,3,1,100,0,now());
//预售表
//增（后台） 删（后台） 改（后台） 查（前台）
主键
用户ID 关联用户表
产品ID 关联产品表
数量
预售价格
预售周期
预售时间
CREATE TABLE presell_cart(
 presell_id  INT PRIMARY KEY AUTO_INCREMENT,
 uid INT,
 pid INT,
 presell_count INT,
 presell_day INT,
presell_price DOUBLE(10,2),
presell_time DATETIME
);
INSERT INTO presell_cart VALUES(null,1,1,1,1,1,now());
INSERT INTO presell_cart VALUES(null,1,2,2,10,2.1,now());
INSERT INTO presell_cart VALUES(null,1,3,3,100,3,now());
INSERT INTO presell_cart VALUES(null,1,4,4,1000,2.54,now());
INSERT INTO presell_cart VALUES(null,1,3,3,100,3,now());
INSERT INTO presell_cart VALUES(null,1,4,4,1000,2.54,now());
INSERT INTO presell_cart VALUES(null,1,3,3,100,3,now());
INSERT INTO presell_cart VALUES(null,1,4,4,1000,2.54,now());
//期号表
//增（后台） 删（后台） 改（后台） 查（前台）
期号ID主键
期号名称
用户ID 关联用户表
产品ID 关联产品表
购买数量
期号时间
CREATE TABLE  product_issue(
 issue_id   INT PRIMARY KEY AUTO_INCREMENT,
Issue_name INT,
 uid INT,
 pid INT,
Buy_count INT,
Issue_time DATETIME
);
INSERT INTO product_issue VALUES(null,1,1,1,4,now());
INSERT INTO product_issue VALUES(null,2,2,2,3,now());
INSERT INTO product_issue VALUES(null,1,1,1,4,now());
INSERT INTO product_issue VALUES(null,2,2,2,3,now());
INSERT INTO product_issue VALUES(null,1,1,1,4,now());
INSERT INTO product_issue VALUES(null,2,2,2,3,now());
//我的福利表
主键
用户ID
适用周期
金额
来源
z状态 未使用  已使用 已过期
类型 体验卷 现金红包 加息卷 减免卷 折扣卷
获取时间
CREATE TABLE  user_welfare(
 wid INT PRIMARY KEY AUTO_INCREMENT,
 uid INT,
apply_cycle INT,
welfare_price INT,
#1 未适用, 2 已使用,3 已过期
c_state tinyint(1),
#1 猪肉代金劵,2 体验劵, 3 现金红包, 4 加息劵, 5 减免劵 6 折扣劵
welfare_class tinyint(1),
form VARCHAR(200),
welfare_time DATETIME
);
INSERT INTO user_welfare VALUES(null,1,1,1,1,6,"微信",now());
INSERT INTO user_welfare VALUES(null,2,2,2,3,1,"微博",now());
INSERT INTO user_welfare VALUES(null,1,1,1,2,2,"淘宝",now());
INSERT INTO user_welfare VALUES(null,2,2,2,4,3,"天猫",now());
INSERT INTO user_welfare VALUES(null,1,1,1,1,4,"京东",now());
INSERT INTO user_welfare VALUES(null,2,2,2,3,5,"联通",now());
//牧场好友表
主键
已邀请的好友 邀请的好友人数
好友
用户ID
好友状态 已注册 未注册
邀请时间
CREATE TABLE  user_frends(
 sid   INT PRIMARY KEY AUTO_INCREMENT,
 uid INT,
 #1 已注册, 2 未注册
 frends_state tinyint(1),
frends_time DATETIME
);
INSERT INTO user_frends VALUES(null,1,1,now());
INSERT INTO user_frends VALUES(null,1,2,now());
INSERT INTO user_frends VALUES(null,1,1,now());
INSERT INTO user_frends VALUES(null,1,2,now());
INSERT INTO user_frends VALUES(null,1,1,now());
INSERT INTO user_frends VALUES(null,1,2,now());






